@ECHO OFF



REM ---
REM Print the cool welcome message...
REM ---
COLOR 4F
ECHO ==========================================================
ECHO ========== WELCOME TO REIMAGINED - BY THE BOYS ===========
ECHO ==========================================================
ECHO Allow us to prepare you for Armageddon...



REM ---
REM Make sure we're in the right place and that game_mod is installed.
REM ---
ECHO.
ECHO ========== 0.) Game Folder Sanity Checks =================
ECHO Checking launcher location...
IF NOT EXIST .\BlackOps.exe (
	ECHO ERROR: BlackOps.exe not found!
	ECHO        Make sure that you have extracted this launcher into your Black Ops game directory.
	PAUSE
	EXIT
)
ECHO Checking for Black Ops Mod Tools...
IF NOT EXIST .\bin\converter.exe (
	ECHO ERROR: bin\converter.exe not found!
	ECHO        Make sure that you have the Black Ops Mod Tools downloaded in your Steam Library!
	ECHO        You should be able to find Black Ops Mod Tools under the "Tools" category.
	PAUSE
	EXIT
)
IF NOT EXIST .\bin\linker_pc.exe (
	ECHO ERROR: bin\linker_pc.exe not found!
	ECHO        Make sure that you have the Black Ops Mod Tools downloaded in your Steam Library!
	ECHO        You should be able to find Black Ops Mod Tools under the "Tools" category.
	PAUSE
	EXIT
)



REM ---
REM Check for game_mod, and install it if not present.
REM ---
SET GModFile=game_mod.zip
SET GModURL=https://github.com/Nukem9/LinkerMod/releases/download/v1.3.2/%GModFile%
ECHO.
ECHO ========== 1.) game_mod Checks ===========================
IF NOT EXIST .\bin\game_mod.dll (
	IF NOT EXIST .\%GModFile% (
		ECHO game_mod has not yet been downloaded!
		ECHO Downloading game_mod now, this may take a minute...
		powershell -Command "Invoke-WebRequest %GModURL% -OutFile %GModFile%"
	) ELSE (
		ECHO game_mod has not yet been extracted!
	)
	IF NOT EXIST .\%GModFile% (
		ECHO ERROR: game_mod could not be successfully downloaded.
		ECHO        If you wish to download it manually, extract the game_mod folder into your Black Ops directory.
		PAUSE
		EXIT
	) ELSE (
		ECHO Extracting game_mod now, this may take a minute...
		powershell Expand-Archive %GModFile% . -Force
		ECHO Cleaning up archive...
		DEL %GModFile%
	)
) ELSE (
	ECHO game_mod located!
)



REM ---
REM Check for Portable Git, and install it if not present.
REM --- 
SET PGitFile=PortableGit-2.25.0-32-bit.7z.exe
SET PGitURL=https://github.com/git-for-windows/git/releases/download/v2.25.0.windows.1/%PGitFile%
ECHO.
ECHO ========== 2.) Portable Git Checks =======================
IF NOT EXIST .\PortableGit\ (
	IF NOT EXIST .\%PGitFile% (
		ECHO Portable Git has not yet been downloaded!
		ECHO Downloading Portable Git now, this may take a minute...
		powershell -Command "Invoke-WebRequest %PGitURL% -OutFile %PGitFile%"
	) ELSE (
		ECHO Portable Git has not yet been extracted!
	)
	IF NOT EXIST .\%PGitFile% (
		ECHO ERROR: Portable Git could not be successfully downloaded.
		ECHO        If you wish to download it manually, extract the PortableGit folder into your Black Ops directory.
		PAUSE
		EXIT
	) ELSE (
		ECHO Extracting Portable Git now, this may take a minute...
		.\%PGitFile% -y
		ECHO Cleaning up installer...
		DEL %PGitFile%
	)
) ELSE (
	ECHO Portable Git located!
)



REM ---
REM Initialize the local repository if necessary.
REM ---
ECHO.
ECHO ========== 3.) Repository Setup ==========================
ECHO Making sure mod directories exist...
IF NOT EXIST .\mods\ (
	ECHO Creating mods folder...
	MKDIR .\mods\
)
IF NOT EXIST .\mods\Reimagined-By-The-Boys\ (
	ECHO Creating Reimagined-By-The-Boys directory...
	MKDIR .\mods\Reimagined-By-The-Boys\
) ELSE (
	ECHO Directories are good!
)
ECHO Entering mod directory...
cd .\mods\Reimagined-By-The-Boys\
IF NOT EXIST .\.git\ (
	ECHO Setting up repository....
	..\..\PortableGit\bin\git.exe init .
	..\..\PortableGit\bin\git.exe remote add origin "https://gitlab.com/jessieh/bo1-reimagined-by-the-boys.git"
	..\..\PortableGit\bin\git.exe config core.autocrlf input
) ELSE (
	ECHO Repository is good to go!
)



REM ---
REM Download and update files as needed.
REM ---
SET RStatusFile=repostatus.tmp
ECHO.
ECHO ========== 4.) File Updates ==============================
ECHO Fetching remote repository changes...
..\..\PortableGit\bin\git.exe fetch --all
ECHO Stashing repository status...
DEL %RStatusFile%
..\..\PortableGit\bin\git.exe reset --soft origin/master
..\..\PortableGit\bin\git.exe status --porcelain > %RStatusFile%
ECHO Applying any remote changes...
..\..\PortableGit\bin\git.exe reset --hard origin/master
ECHO All up to date!



REM ---
REM Build file index and link FastFile.
REM ---
SET /P RStatus=<%RStatusFile%
DEL %RStatusFile%
ECHO.
ECHO ========== 5.) Build File Index ==========================
ECHO Checking stashed repository status...
IF DEFINED RStatus (
	ECHO Repository has been updated! Initiating rebuild...
	ECHO Building mod file index....
	..\..\PortableGit\git-bash.exe -c "git ls-files | grep -vf .buildexcludes > Reimagined-By-The-Boys.files"
	ECHO Linking fast file -- you can ignore the errors here....
	cd ..\..\bin\
	.\linker_pc.exe -nopause -language english -moddir Reimagined-By-The-Boys mod
	move ..\zone\English\mod.ff ..\mods\Reimagined-By-The-Boys\mod.ff
	ECHO Converting any .GDTs to game data...
	.\converter.exe -nopause -n -nospam
) ELSE (
	cd ..\..\bin\
	ECHO Mod file index up to date!
)



REM ---
REM Launch the mod!
REM ---
ECHO.
ECHO ========== 6.) Launch ====================================
START launcher_ldr.exe game_mod.dll ..\BlackOps.exe +set fs_game "mods/Reimagined-By-The-Boys"