# Call of Duty: Black Ops Zombies - Reimagined - By The Boys

## Launcher - By The Boys

Place "BO_Reimagined-By-The-Boys.bat" in your Black Ops game directory and run it.

The launcher will automatically download and extract everything that is needed to play Reimagined - By The Boys.
Mod updates will be downloaded automatically whenever you launch the file after it has performed the initial setup.

Enjoy!

-The Boys